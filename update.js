const dbConnect = require("./mongodb");

const updateData = async () => {
  const db = await dbConnect();
  const update = await db.updateOne(
    { brand: "dell" },
    { $set: { brand: "acer" } }
  );
  console.log(update);
};

updateData();
