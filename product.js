const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const productModel = mongoose.model;

const productSchema = new Schema({
  name: String,
  price: Number,
  brand: String,
  category: String,
});

const productInstance = productModel("products", productSchema);

module.exports = productInstance;
