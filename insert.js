const dbConnect = require("./mongodb");

const insertData = async () => {
  const db = await dbConnect();
  const insert = await db.insert({
    name: "dell adventure",
    price: 54000,
    brand: "dell",
    category: "laptop",
  });
  console.log(insert);
  if (insert.acknowledged) {
    console.log("Data is inserted");
  }
};

insertData();
