const express = require("express");
const dbConnect = require("./mongoConfig");
const http = require("http");
const mongodb = require("mongodb");
const app = express();
app.use(express.json());

app.get("/", async (req, res) => {
  let data = await dbConnect();
  result = await data.find().toArray();
  console.log(result);
  res.send(result);
});

app.post("/", async (req, res) => {
  let data = await dbConnect();
  let result = await data.insert(req.body);
  res.send(result);
});

// here brand is an unique key to update data
app.put("/:brand", async (req, res) => {
  console.log(req.body);
  let data = await dbConnect();
  let result = await data.updateOne(
    { brand: req.params.brand },
    { $set: req.body }
  );
  res.send(result);
});

// here brand is an unique key to delete data from collection of products
app.delete("/:id", async (req, res) => {
  let data = await dbConnect();
  //   let result = await data.deleteOne({ brand: req.params.brand }); or..>>>>>>>>>
  let result = await data.deleteOne({
    _id: new mongodb.ObjectId(req.params.id),
  });
  console.log(result);
  res.send(result);
});
app.listen(5000);
