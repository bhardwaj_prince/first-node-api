const express = require("express");
require("./mongooseConfig");
const product = require("./product");
const app = express();
app.use(express.json());
const mongodb = require("mongodb");

app.post("/create", async (req, res) => {
  let data = new product(req.body);
  const result = await data.save();
  res.send(result);
  console.log(result);
});

app.get("/", async (req, res) => {
  let data = await product.find({ req });
  res.send(data);
  console.log(data);
});

app.get("/:brand", async (req, res) => {
  let data = await product.findOne(req.params);
  res.send(data);
  console.log(data);
});

app.put("/:brand", async (req, res) => {
  let data = await product.updateOne(
    { brand: req.params.brand },
    {
      $set: {
        name: "jio",
        price: "25000",
      },
    }
  );
  res.send(data);
  console.log("data updated Successfully", data);
});

app.delete("/:name", async (req, res) => {
  let data = await product.deleteOne({
    name: req.params.name,
  });
  res.send(data);
  console.log("data Deleted Successfully", data);
});

app.listen(7000);
