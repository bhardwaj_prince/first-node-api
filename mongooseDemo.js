const mongoose = require("mongoose");
const url = "mongodb://localhost:27017/e-comm";
const Schema = mongoose.Schema;

mongoose.connect(url);
//   Create Schema for products
const productSchema = new Schema({
  name: String,
  price: Number,
  brand: String,
  category: String,
});

//connect schema with product collection present in e-comm database
const instance = mongoose.model("products", productSchema);

saveInDb = async () => {
  //   Assign New dataObject to model product
  let data = new instance({
    name: "edge 20",
    price: 19999,
    brand: "moto",
    category: "mobile",
  });
  const result = await data.save();
  console.log("mongoose connected successfully", result);
};

updateInDb = async () => {
  let data = await instance.updateOne(
    { brand: "moto" },
    {
      $set: {
        name: "poco m2",
        price: 19999,
        brand: "pocco",
        category: "mobile",
      },
    }
  );
  console.log("data updated successfully ", data);
};

deleteInDb = async () => {
  let data = await instance.deleteOne({
    brand: "moto",
  });
  console.log("data deleted successfully", data);
};

findInDb = async () => {
  let data = await instance.findOne({ brand: "pocco" });
  console.log("data find successfully", data);
};
// saveInDb();
// updateInDb();
// deleteInDb();
findInDb();
